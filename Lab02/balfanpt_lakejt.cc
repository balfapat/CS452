/*
* By Patrick Balfanz and Jason Lake 
*/
#include <iostream>
#include <thread>
#include <cstdlib>
#include <mutex>
#include <bits/stdc++.h> 
using namespace std;

//mutex for locking 
std::mutex mtx;
//Bank account shared resource 
int bankAccount = 400;

//Random function
long f(long nonce)
{
    const long A = 48271;
    const long M = 2147483647;
    const long Q = M / A;
    const long R = M % A;

    static long state = 1;
    long t = A * (state % Q) - R * (state / Q);

    if (t > 0)
        state = t;
    else
        state = t + M;
    return (long)(((double)state / M) * nonce);
}

//Method each thread calls to determine trans/withdr behavior
void transaction(int numTrans){
    std::thread::id this_id = std::this_thread::get_id();
    //Loop for the number of transactions 
    for (int i = 0; i < numTrans; i++){
        //Create the random seed 
        long seed = time(NULL);
        //Determine cash amount and withdraw rands
        int cashAmount = f(seed) % 300 + 1;
        double withOrDep = ((double) f(seed) / seed); // 0 - 100
        
        //Critical Section
        mtx.lock();
        //withdraw if rand rum is >.5
        if (withOrDep > .5){
            //Do not withdrawl if not enough funds
            if(cashAmount > bankAccount){
                 cout << "tID: " << this_id << " - balance = $" << bankAccount << ".00 | withdraw $" << cashAmount << ".00 (NSF) | balance = $" << bankAccount << ".00 | timestamp: "<< time(NULL) << endl;
            }else{
                cout << "tID: " << this_id << " - balance = $" << bankAccount << ".00 | withdraw $" << cashAmount << ".00 | balance = $" << bankAccount - cashAmount << ".00 | timestamp: "<< time(NULL) << endl;
                bankAccount -= cashAmount;
            }
        }
        //Deposit if rand num <= .5
        else{
            cout << "tID: " << this_id << " - balance = $" << bankAccount << ".00 | deposit $" << cashAmount << ".00 | balance = $" << bankAccount + cashAmount << ".00 | timestamp: "<< time(NULL) << endl;
            bankAccount += cashAmount;
        }
        //end of critical section
       mtx.unlock();
    }
}

int main(){
   //initialize rand function 
    f(time(NULL));
    cout << "Enter the number of threads: ";
    int numthreads = 1;
    cin >> numthreads;
    //Maximum amount of threads for the thingys
    while(numthreads > 30098){
        cout << "Too many threads. Enter a new number: ";
        cin >> numthreads;
    }
    int numTrans = 1;
    cout << "Enter the number of transactions per thread: ";
    cin >> numTrans;
    //Create threads
    thread threads[numthreads];
    for (size_t i = 0; i < numthreads; i++)
    {
        threads[i] = std::thread(transaction, numTrans);
    }
    //Join threads to the main
    for (size_t i = 0; i < numthreads; i++)
    {
        threads[i].join();
    }
    return 0;
}