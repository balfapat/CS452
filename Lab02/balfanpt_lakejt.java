/*
* By Patrick Balfanz and Jason Lake 
*/
import java.util.*;
class BankThread implements Runnable {
    //initialize variables and objects
    private int numTransactions;
    BankAccount account;
    Random rand;

    public BankThread(int numTransactions, BankAccount account, Random rand) {
        this.numTransactions = numTransactions;
        this.account = account;
        this.rand = rand;
    }
    //Update balance for each transaction
    void updateBalance(int cashAmount, double withOrDep) {
        //Synchronized based off account to prevent thread issues
        synchronized (account) {
            if(withOrDep > .5){
                if(cashAmount > account.balance){
                    System.out.println("tID: " + Thread.currentThread().getId() + " - balance = $" + account.balance + ".00 | withdraw $" + cashAmount + ".00 (NSF) | balance = $" + account.balance + ".00 | timestamp: " +  new Date().getTime());
                }else{
                    System.out.println("tID: " + Thread.currentThread().getId() + " - balance = $" + account.balance + ".00 | withdraw $" + cashAmount + ".00 | balance = $" + (account.balance - cashAmount) + ".00 | timestamp: " + new Date().getTime());
                    account.balance -= cashAmount;
                }
            }else{
                System.out.println("tID: " + Thread.currentThread().getId() + " - balance = $" + account.balance + ".00 | deposit $" + cashAmount + ".00 | balance = $" + (account.balance + cashAmount) + ".00 | timestamp: " + new Date().getTime());
                account.balance += cashAmount;
            }
        }
    }

    @Override
    public void run() {
        for (int i = 0; i < numTransactions; i++) {
            updateBalance(rand.getCashAmount(), rand.getDepositOrWithdrawal());
        }
    }
}
//Random class derived from the f() function. Variables were pulled out to ensure it
//behaves the same as the c version. 
class Random{
    //variables used in the original f()
    private final long A = 48271;
    private final long M = 2147483647;
    private final long Q = M / A;
    private final long R = M % A;   
    private long state = 1;

    private long seed;
    //Creates the random generator based off a given seed number
    public Random(long seed){
        this.seed = seed;
        f(seed);
    }
    private long f(long nonce){
        long t = A * (state % Q) - R * (state / Q);
        if (t > 0)
            state = t;
        else
            state = t + M;
        return (long)(((double)state / M) * nonce);
    }
    //Returns a random double [0 - 1]
    public double getDepositOrWithdrawal(){
        return ((double) f(seed) / seed);
    }
    //Returns a random int [1 - 300]
    public int getCashAmount(){
        return (int) (f(seed) % 300 + 1);
    }
}
//Bank account data to be shared between threads
class BankAccount {
    public int balance;
    public BankAccount(int balance) {
        this.balance = balance;
    }
}

class balfanpt_lakejt {
    public static void main(String[] args) throws InterruptedException {
        Scanner scan = new Scanner(System.in);
        //Create a bank and rand objects to be shared by all threads.
        BankAccount bank = new BankAccount(400);
        Random rand = new Random(new Date().getTime());

        System.out.print("Enter the number of threads: ");
        int numThreads = scan.nextInt();
        //24 is the maximum amount of threads on the things
        while(numThreads > 24){
            System.out.print("Too many threads. Enter a new number: ");
            numThreads = scan.nextInt();
        }
        System.out.print("Enter the number of transactions per thread: ");
        int numProcesses = scan.nextInt();
        scan.close();
        //Create the threads start them based on the number of processes and
        //the shared bank and rand objects
        Thread threads[] = new Thread[numThreads];
        for (int i = 0; i < numThreads; i++) {
            threads[i] = new Thread(new BankThread(numProcesses, bank, rand));
            threads[i].start();
        }
        //Join the threads back to main
        for (int i = 0; i < numThreads; i++) {
            threads[i].join();
        }
    }
}