#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <stdio.h>      /* for printf() and fprintf() */
#include <sys/types.h>  /* for Socket data types */
#include <sys/socket.h> /* for socket(), connect(), send(), and recv() */
#include <netinet/in.h> /* for IP Socket data types */
#include <arpa/inet.h>  /* for sockaddr_in and inet_addr() */
#include <stdlib.h>     /* for atoi() */
#include <string.h>     /* for memset() */
#include <unistd.h>     /* for close() */
#include <vector>
#include <iostream>
#include "blowfish/blowfishKDC1.h"
#include "blowfish/blowfishKDC2.h"
#define MAX 80
#define PORT 9104
#define SA struct sockaddr

#define DEBUG

using namespace std;

// Function designed for chat between KDC and A.
void func(int sockfd)
{
    string ka;
    string ks;
    string kb;
    string idA;

    //user entered input
    cout << "Please enter User A's private key: ";
	cin >> ka;
	if (ka.length() % 2 != 0) {
		cout << "Keys must be an even length. Appending \"!\"." << endl;
		ka += "!";
	}
	cout << "Please enter User B's private key: ";
	cin >> kb;
	if (kb.length() % 2 != 0) {
		cout << "Keys must be an even length. Appending \"!\"." << endl;
		kb += "!";
	}
	cout << "Please enter the session key: ";
	cin >> ks;
	if (ks.length() % 2 != 0) {
		cout << "Keys must be an even length. Appending \"!\"." << endl;
		ks += "!";
	}

    // Receive the requset from A
    int requestLength;
    recv(sockfd, &requestLength, sizeof(requestLength), 0);
    char *buff = new char[1000];
    recv(sockfd, buff, requestLength, 0);

    //Convert the nonce into an unsigned long int
    string rec = buff;
    char buffer[256];
    string nonceStr = rec.substr(rec.find("= ") + 2);
    strcpy(buffer, nonceStr.c_str());
    unsigned long int nonce = strtoul(buffer, NULL, 0);
    string request = rec.substr(0, rec.find("N1"));

    #ifdef DEBUG
        cout << endl;
        cout << "Recd from IDa:" << endl; 
        cout << "Requesting Ks for (IDb)" << endl;
        cout << "N1 recd = " << nonce << endl;
        cout << endl;
    #endif

    //Encrypt and send EKa[Ks||Req||N1||EKb(Ks, IDA)]:  to A
    BLOWFISHKDC1 bfks(kb);
    string eks = bfks.Encrypt_CBC(ks + "," + idA);

    BLOWFISHKDC2 bfka(ka);
    string eka = bfka.Encrypt_CBC(ks + "," + request + "," + to_string(nonce) + "," + eks);

    #ifdef DEBUG
        cout << "Send to IDa:" << endl; 
        cout << "EKa[Ks||Req||N1||EKb(Ks, IDA)]: " << eka.substr(0,6) << "..."<< endl;
        cout << "Ks = " << ks << endl;
        cout << "N1 = " << nonce << endl;
        cout << endl;
    #endif


    int ekaLength = eka.length();

    char *ekaBuff = new char[1000];
    strcpy(ekaBuff, eka.c_str());
    send(sockfd, &ekaLength, sizeof(ekaLength), 0);
    send(sockfd, ekaBuff, ekaLength, 0);
    delete[] buff;
    delete[] ekaBuff;
}
// Driver function
int main()
{

    #ifdef DEBUG
        cout << "RUNNING DEBUG" << endl;
    #endif


    int sockfd, connfd;
    struct sockaddr_in servaddr, cli;

    // socket create and verification
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1)
    {
        printf("socket creation failed...\n");
        exit(0);
    }
    else
        printf("Socket successfully created..\n");
    bzero(&servaddr, sizeof(servaddr));

    // assign IP, PORT
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    //servaddr.sin_addr.s_addr = htonl("192.168.56.1");
    servaddr.sin_port = htons(PORT);

    // Binding newly created socket to given IP and verification
    if ((bind(sockfd, (SA *)&servaddr, sizeof(servaddr))) != 0)
    {
        printf("socket bind failed...\n");
        exit(0);
    }
    else
        printf("Socket successfully binded..\n");

    // Now server is ready to listen and verification
    if ((listen(sockfd, 5)) != 0)
    {
        printf("Listen failed...\n");
        exit(0);
    }
    else
        printf("Server listening..\n");
    socklen_t len = sizeof(cli);

    // Accept the data packet from client and verification
    connfd = accept(sockfd, (SA *)&cli, &len);
    if (connfd < 0)
    {
        printf("server acccept failed...\n");
        exit(0);
    }
    else
        printf("server acccept the client...\n");

    // Function for chatting between client and server
    func(connfd);

    // After chatting close the socket
    close(sockfd);
}
