#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <stdio.h>      /* for printf() and fprintf() */
#include <sys/types.h>  /* for Socket data types */
#include <sys/socket.h> /* for socket(), connect(), send(), and recv() */
#include <netinet/in.h> /* for IP Socket data types */
#include <arpa/inet.h>  /* for sockaddr_in and inet_addr() */
#include <stdlib.h>     /* for atoi() */
#include <string.h>     /* for memset() */
#include <unistd.h>     /* for close() */
#include <vector>
#include <iostream>
#include <sstream>
#include "blowfish/blowfishB1.h"
#include "blowfish/blowfishB2.h"
#include "blowfish/blowfishB3.h"
#include <bits/stdc++.h>
#define MAX 80
#define PORT 9104
#define SA struct sockaddr

#define DEBUG

using namespace std;

long f(long nonce)
{
    const long A = 48271;
    const long M = 2147483647;
    const long Q = M / A;
    const long R = M % A;

    static long state = 1;
    long t = A * (state % Q) - R * (state / Q);

    if (t > 0)
        state = t;
    else
        state = t + M;
    return (long)(((double)state / M) * nonce);
}

//Used for parsing a string 
std::vector<std::string> split(const std::string &s, char delim)
{
    std::stringstream ss(s);
    std::string item;
    std::vector<std::string> elems;
    while (std::getline(ss, item, delim))
    {
        elems.push_back(item);
        // elems.push_back(std::move(item)); // if C++11 (based on comment from @mchiasson)
    }
    return elems;
}

//calls recv until the buffer of the specified size is completly filled
//Ensures all data sent by send() has been received 
void recvBuff(int sockfd, char *buffer, int size, int flag)
{
    int recvBytes = recv(sockfd, buffer, size, 0);
    int total = recvBytes;
    while (total < size)
    {
        int bytes = recv(sockfd, buffer + total, size - total, 0);
        total += bytes;
    }
}

//communication between Initiator A and the Responder B
void func(int sockfd)
{
    string kb;
    unsigned long int n2;

    //User entered data -> Kb and N2
    cout << "Please enter User B's private key: ";
    cin >> kb;
    if (kb.length() % 2 != 0)
    {
        cout << "Keys must be an even length. Appending \"!\"." << endl;
        kb += "!";
    }
    cout << "Please enter a number: ";
    cin >> n2;

    //propmt the user continuously until it gets a valid number
    while (!cin.good())
    {
        cin.clear();
        cin.ignore(ULONG_MAX, '\n');
        cout << "Please enter a valid number: ";
        cin >> n2;
    }

    //Reveive Ekb[Ks,IDa] from A and convert to a string
    char *ekbBuff = new char[10000];
    int ekbLength;
    recv(sockfd, &ekbLength, sizeof(ekbLength), 0);
    recvBuff(sockfd, ekbBuff, ekbLength, 0);
    string ekb = ekbBuff;

    //Decrypt Ekb[Ks,IDa] and parse for Ks
    BLOWFISHB1 bfkb(kb);
    string dkb = bfkb.Decrypt_CBC(ekb);
    vector<string> dkbSplit = split(dkb, ',');
    string ks = dkbSplit[0];

    #ifdef DEBUG
        cout << endl;
        cout << "Recd from IDa:" << endl;
        cout << "Ekb[Ks,IDa]: " << ekb.substr(0, 6) << "..." << endl;
        cout << "Ks = " << ks << endl;
        cout << "N2 = " << n2 << endl;
        cout << endl;
    #endif

    //Encrypt Eks[N2] and send it to A
    BLOWFISHB2 bfks(ks);
    string en2 = bfks.Encrypt_CBC(to_string(n2));
    int en2length = en2.length();
    char *en2Buff = new char[10000];
    strcpy(en2Buff, en2.c_str());
    send(sockfd, &en2length, sizeof(en2length), 0);
    send(sockfd, en2Buff, en2length, 0);

    #ifdef DEBUG
        cout << "Send to IDa:" << endl;
        cout << "Eks[N2]: " << en2.substr(0, 6) << "..." << endl;
        cout << endl;
    #endif

    //Decrypt f(N2) and convert to a string
    char *efn2Buff = new char[10000];
    int efn2Length;
    recv(sockfd, &efn2Length, sizeof(efn2Length), 0);
    recvBuff(sockfd, efn2Buff, efn2Length, 0);
    string efn2 = efn2Buff;
    

    //convert string to unsigned long int
    char ulibuffer[256];
    strcpy(ulibuffer, bfks.Decrypt_CBC(efn2).c_str());
    unsigned long int fn2Received = strtoul(ulibuffer, NULL, 0);

    //calculate f(N2) for comparison
    unsigned long int fn2 = f(n2);

    #ifdef DEBUG
        cout << "Recd from IDa:" << endl;
        cout << "Eks[f(N2)]:" << efn2.substr(0, 6) << "..." << endl;
        cout << "f(N2) = " << fn2 << endl;
        cout << "Validate (Recd f(N12) == calc f(N2)): " << to_string(fn2Received) << " == "<< to_string(fn2) << endl;
        cout << endl;
    #endif

    //f(N2) validation
    if (fn2 != fn2Received)
    {
        cout << "Could not validate" << endl;
        return;
    }
    else
    {
        cout << "Successfully validated" << endl;
    }
    //free up memory created by the buffers
    delete[] ekbBuff;
    delete[] en2Buff;
    delete[] efn2Buff;

    //Recieve the file name from A
    int fileNameLength;
    recv(sockfd, &fileNameLength, sizeof(fileNameLength), 0);
    char *fileNameBuff = new char[fileNameLength];
    recv(sockfd,fileNameBuff,fileNameLength,0);
    string fileName = fileNameBuff;
    delete[] fileNameBuff;  

    //extracts file name to send
    size_t found = fileName.find_last_of("/");
    string file;
    if(found != string::npos) {
        file = fileName.substr(found + 1);
    } else {
        file = fileName;
    }

    //finds the file extension
    string extension;
    size_t found2 = file.find(".");
    if(found2 != string::npos) {
        extension = file.substr(found);
    } else {
        extension = "";
    }

    //recieve file data from A
    int bufferSize;
    int nd1 = recv(sockfd, &bufferSize, sizeof(bufferSize), 0);
    int numBlocks;
    int nd2 = recv(sockfd, &numBlocks, sizeof(numBlocks), 0);
    int leftover;
    int nd3 = recv(sockfd, &leftover, sizeof(leftover), 0);

    // create output file
    string outName = "/tmp/Copy-";
    outName.append(file);
    std::ofstream outfile(outName, std::ofstream::binary);

    //Create a buffer that can hold the encrypted data
    //The encrypted data will always be a larger length than the unencrypted 
    int buf = bufferSize * 1.5;
    //create a shared buffer so we don't waste space
    //This will get overwritten for every block sent
    byte *buffer = new byte[buf];
    string encryptHex;
    string decryptHex;
    cout << "Receiving File..." << endl;
    for (size_t i = 0; i < numBlocks; i++)
    {
        int encryptedSize;
        recv(sockfd, &encryptedSize, sizeof(encryptedSize), 0);

        //Keep calling recv() until the buffer is filled
        int recvBytes = recv(sockfd, buffer, encryptedSize, 0);
        int total = recvBytes;
        while (total < encryptedSize)
        {
            int bytes = recv(sockfd, buffer + total, encryptedSize - total, 0);
            total += bytes;
        }

        //decrypt and write out to a file
        int decryptSize;
        byte *decrypt = bfks.Decrypt_CBC(buffer, encryptedSize, &decryptSize);

        if(i == 0){
            #ifdef DEBUG
                for (int i = 0 ; i < 3; i++){
                     encryptHex += bfks.byteToHex(buffer[i]);
                }
                for (int i = 0 ; i < 3; i++){
                     decryptHex += bfks.byteToHex(decrypt[i]);
                }
        #endif
        }
        outfile.write((char *)decrypt, decryptSize);
        delete[] decrypt;
    }
    delete[] buffer;

    outfile.close();

    cout << endl;
    cout << "File received and creatd at: " << outName << endl;
    cout << "Encrypted file converted to hex: " << endl;
    cout << encryptHex << "..." << endl;
    cout << "File converted to hex: " << endl;
    cout << decryptHex << "..." << endl;
}
// Driver function
int main()
{

    #ifdef DEBUG
        cout << "RUNNING DEBUG" << endl;
    #endif


    int sockfd, connfd;
    struct sockaddr_in servaddr, cli;

    // socket create and verification
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1)
    {
        printf("socket creation failed...\n");
        exit(0);
    }
    else
        printf("Socket successfully created..\n");
    bzero(&servaddr, sizeof(servaddr));

    // assign IP, PORT
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    //servaddr.sin_addr.s_addr = htonl("192.168.56.1");
    servaddr.sin_port = htons(PORT);

    // Binding newly created socket to given IP and verification
    if ((bind(sockfd, (SA *)&servaddr, sizeof(servaddr))) != 0)
    {
        printf("socket bind failed...\n");
        exit(0);
    }
    else
        printf("Socket successfully binded..\n");

    // Now server is ready to listen and verification
    if ((listen(sockfd, 5)) != 0)
    {
        printf("Listen failed...\n");
        exit(0);
    }
    else
        printf("Server listening..\n");
    socklen_t len = sizeof(cli);

    // Accept the data packet from client and verification
    connfd = accept(sockfd, (SA *)&cli, &len);
    if (connfd < 0)
    {
        printf("server acccept failed...\n");
        exit(0);
    }
    else
        printf("server acccept the client...\n");

    // Function for chatting between client and server
    func(connfd);

    // After chatting close the socket
    close(sockfd);
}
