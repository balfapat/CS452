// Write CPP code here
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <stdio.h>      /* for printf() and fprintf() */
#include <sys/types.h>  /* for Socket data types */
#include <sys/socket.h> /* for socket(), connect(), send(), and recv() */
#include <netinet/in.h> /* for IP Socket data types */
#include <arpa/inet.h>  /* for sockaddr_in and inet_addr() */
#include <stdlib.h>     /* for atoi() */
#include <string.h>     /* for memset() */
#include <unistd.h>     /* for close() */
#include <vector>
#include <iostream>
#include <bits/stdc++.h>
#include <math.h>
#include "blowfish/blowfishA1.h"
#include "blowfish/blowfishA2.h"
#include <string>
#include <sstream>
#include <vector>
#include <limits>
#define MAX 80
#define BLOCKSIZE 1000000
#define PORT 9104
#define SA struct sockaddr


#define DEBUG 
using namespace std;
long f(long nonce)
{
    const long A = 48271;
    const long M = 2147483647;
    const long Q = M / A;
    const long R = M % A;

    static long state = 1;
    long t = A * (state % Q) - R * (state / Q);

    if (t > 0)
        state = t;
    else
        state = t + M;
    return (long)(((double)state / M) * nonce);
}

//Used for parsing a string 
std::vector<std::string> split(const std::string &s, char delim)
{
    std::stringstream ss(s);
    std::string item;
    std::vector<std::string> elems;
    while (std::getline(ss, item, delim))
    {
        elems.push_back(item);
        // elems.push_back(std::move(item)); // if C++11 (based on comment from @mchiasson)
    }
    return elems;
}

//calls recv until the buffer of the specified size is completly filled
//Ensures all data sent by send() has been received 
void recvBuff(int sockfd, char *buffer, int size, int flag){
    int recvBytes = recv(sockfd, buffer, size, 0);
        int total = recvBytes;
        while (total < size)
        {
            int bytes = recv(sockfd, buffer + total, size - total, 0);
            total += bytes;
        }
}

//communication between Initiator A and the KDC
void funckdc(int sockfd, string &ks, string &ekb)
{
    string ka;
    unsigned long int n1 = 5647892341;

    //User entered data -> Ka and N1
    cout << "Please enter User A's private key: ";
	cin >> ka;
	if (ka.length() % 2 != 0) {
		cout << "Keys must be an even length. Appending \"!\"." << endl;
		ka += "!";
	}
	cout << "Please enter a number: ";
	cin >> n1;
    
    //propmt the user continuously until it gets a valid number
    while (!cin){
        cin.clear();
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
        cout << "Please enter a valid number: ";
        cin >> n1;
    }

    string request = "Ks for (IDB),N1 = " + to_string(n1);
    int requestLength = request.length();

    #ifdef DEBUG
        cout << endl;
        cout << "Send to KDC:" << endl;
        cout << "Request:" << endl;
        cout << "Ks for (IDB)" << endl;
        cout << "N1 = " <<  n1 << endl;
        cout << endl;
    #endif

    //copy the request into a buffer and send it to the kdc
    char *buff = new char[1000];
    strcpy(buff, request.c_str());
    send(sockfd, &requestLength, sizeof(requestLength), 0);
    send(sockfd, buff, requestLength, 0);

    //recieve EKa[Ks||Req||N1||EKb(Ks, IDA)] from A and copy it to a buffer 
    char *ekaBuff = new char[1000];
    int ekaLength;
    recv(sockfd, &ekaLength, sizeof(ekaLength), 0);
    recvBuff(sockfd, ekaBuff, ekaLength, 0);

    // decrypy EKa[Ks||Req||N1||EKb(Ks, IDA)]
    string eka = ekaBuff;
    BLOWFISHA1 bfka(ka);
    string dka = bfka.Decrypt_CBC(eka);

    //Split EKa[Ks||Req||N1||EKb(Ks, IDA)] to extract EKb(Ks, IDA)]
    vector<string> dkaSplit = split(dka, ',');
    ks = dkaSplit[0];
    ekb = dkaSplit.back();

    #ifdef DEBUG
        cout << "Received from KDC:" << endl;
        cout << "EKa[Ks||Req||N1||EKb(Ks, IDA)]: " << eka.substr(0,6) <<"..." << endl;
        cout << "Ks: " << ks << endl;
        cout << "N1: " << dkaSplit[3] << endl;
        cout << endl;
    #endif

    //copy EKb(Ks, IDA) to a buffer and send it to A
    int ekbLength = ekb.length();
    char *ekbBuff = new char[1000];
    strcpy(ekbBuff, ekb.c_str());
    send(sockfd, &ekbLength, sizeof(ekbLength), 0);
    send(sockfd, ekbBuff, ekbLength, 0);

    //free up memory created by the buffers
    delete[] buff;
    delete[] ekaBuff;
    delete[] ekbBuff;
}

// Communication between Initiator A and Responder B
void func(int sockfd, string ks, string ekb)
{

    #ifdef DEBUG
        cout << endl;
        cout << "Send to IDb:" << endl;
        cout << "Ekb[Ks,IDa]: " << ekb.substr(0,6) << "..." << endl;
        cout << endl;
    #endif
    
    //Copy Ekb[Ks,IDa] into a buffer and send it to B
    char *ekbBuff = new char[10000];
    int ekbLength = ekb.length();
    strcpy(ekbBuff, ekb.c_str());
    send(sockfd, &ekbLength, sizeof(ekbLength), 0);
    send(sockfd, ekbBuff, ekbLength, 0);

    //Recieve Eks[N2] from B and convert the recd buffer into a string 
    char *en2Buff = new char[10000];
    int en2length;
    recv(sockfd, &en2length, sizeof(en2length), 0);
    recv(sockfd, en2Buff, en2length, 0);
    string en2 = en2Buff;

    //Decrypt Eks[N2] and calculate f(N2)
    BLOWFISHA2 bfks(ks);
    // //unsigned long int n2 = stoi(bfks.Decrypt_CBC(en2));
    // unsigned long int fn2 = f(n2);


    char ulibuffer[256];
    strcpy(ulibuffer, bfks.Decrypt_CBC(en2).c_str());
    unsigned long int n2 = strtoul(ulibuffer, NULL, 0);
    unsigned long int fn2 = f(n2);

    #ifdef DEBUG
        cout << "Recd from IDb:" << endl;
        cout << "Eks[N2]: " << en2.substr(0,6) << "..." << endl;
        cout << "N2 = " << n2 << endl;
        cout << "f(N2) = " << fn2 << endl;
        cout << endl;
    #endif

    //Encrypt f(N2) and sent it to B 
    string efn2 = bfks.Encrypt_CBC(to_string(fn2));
    int efn2Length = efn2.length();
    char *efn2Buff = new char[1000];
    strcpy(efn2Buff, efn2.c_str());
    send(sockfd, &efn2Length, sizeof(efn2Length), 0);
    send(sockfd, efn2Buff, efn2Length, 0);

    #ifdef DEBUG
        cout << "Send to IDb:" << endl;
        cout << "Eks[f(N2)]: " << efn2.substr(0,6) << "..." << endl;
        cout << endl;
    #endif
    // at this point, respB has validated sucessfully
    //free up memory created by the buffers
    delete[] en2Buff;
    delete[] ekbBuff;
    delete[] efn2Buff;
    
    //User enteres the file to be transferred 
    string fileName;
    cout << "Select a file to encrypt and send. Use the full path name: ";
	cin >> fileName;
	bool fileExists = false;
	while(!fileExists) {
		if(ifstream(fileName).good()) {
			fileExists = true;
		}
		else {
            cout << "File does not exist." << endl;
			cout << "Select a file to encrypt and send. Use the full path name: ";
			cin >> fileName;
		}
	}
  
    // send the file name to B
    int fileNameLength = fileName.length();
    send(sockfd, &fileNameLength, sizeof(fileNameLength), 0);
    char *fileNameBuff = new char[fileNameLength];
    strcpy(fileNameBuff, fileName.c_str());
    send(sockfd, fileNameBuff, fileNameLength, 0);

    delete[] fileNameBuff;

    //opens the file stream and determines size of the file
    std::ifstream infile(fileName, std::ifstream::binary);
    infile.seekg(0, infile.end);
    long size = infile.tellg();
    infile.seekg(0);

    //determine Buffersize, how many pakcets need to be sent over, and the amount of data leftover
    //send this data to B
    int bufferSize = BLOCKSIZE;
    int numBlocks = size / bufferSize + 1;
    int leftover = size % bufferSize;
    send(sockfd, &bufferSize, sizeof(bufferSize), 0);
    send(sockfd, &numBlocks, sizeof(numBlocks), 0);
    send(sockfd, &leftover, sizeof(leftover), 0);

    //create a shared buffer so we don't waste space
    //This will get overwritten for every block sent
    cout << "Transferring..." << endl;
    char *buffer = new char[bufferSize];
    for (size_t i = 0; i < numBlocks; i++)
    {
        //if it is the last block, only send the leftover
        if (i == numBlocks - 1)
        {
            bufferSize = leftover;
        }
        //read the file in a BLOCKSIZE chunk
        infile.read(buffer, bufferSize);
        //encrypt the read chunk
        int newSize;
        byte *encryptedBytes = bfks.Encrypt_CBC((byte *)buffer, bufferSize, &newSize);
    
        if(i == 0){
            #ifdef DEBUG
                cout << "File Converted to hex: " << endl;
                std::string encoded = "";
                for (int i = 0 ; i < 3; i++){
                     encoded += bfks.byteToHex(((byte *)buffer)[i]);
                }
                cout << encoded << "..."<< endl;
                cout << "Encrypted File Converted to hex: " << endl;
                std::string encoded2 = "";
                for (int i = 0 ; i < 3; i++){
                     encoded2 += bfks.byteToHex(encryptedBytes[i]);
                }
                cout << encoded2 << "..." << endl;
        #endif
        }
        //Send the encrypted chunk to B
        send(sockfd, &newSize, sizeof(newSize), 0);
        send(sockfd, encryptedBytes, newSize, 0);
        //Free the encrypted buffer
        delete[] encryptedBytes;
    }
    //free the read in buffer
    delete[] buffer;
    infile.close();

    cout << "Done." << endl;
}

int main()
{
    #ifdef DEBUG
        cout << "RUNNING DEBUG" << endl;
    #endif

    int sockfdkdc, connfdkdc;
    struct sockaddr_in kdcaddr, clikdc;

    // socket create and verification
    sockfdkdc = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfdkdc == -1)
    {
        printf("socket creation failed...\n");
        exit(0);
    }
    else
        printf("Socket successfully created..\n");
    bzero(&kdcaddr, sizeof(kdcaddr));

    string choice1;
    string ip1;
    //Assumes KDC is on thing0
    ip1 = "10.35.195.46";     //thing0
    // ip1 = "10.35.195.47";    //thing1
    // ip1 = "10.35.195.22";    //thing2
    // ip1 = "10.35.195.49";    //thing3
    // ip1 = "127.0.0.1";       //localhost
  
    // assign IP, PORT
    kdcaddr.sin_family = AF_INET;
    kdcaddr.sin_addr.s_addr = inet_addr(ip1.c_str());
    kdcaddr.sin_port = htons(PORT);

    // connect the client socket to server socket
    if (connect(sockfdkdc, (SA *)&kdcaddr, sizeof(kdcaddr)) != 0)
    {
        printf("connection with the server failed...\n");
        exit(0);
    }
    else
        printf("connected to the server..\n");

    //return by reference the session key and ekb
    string ks;
    string ekb;
    funckdc(sockfdkdc, ks, ekb);
    close(sockfdkdc);

    //connection to responder B
    int sockfd, connfd;
    struct sockaddr_in servaddr, cli;

    // socket create and verification
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1)
    {
        printf("socket creation failed...\n");
        exit(0);
    }
    else
        printf("Socket successfully created..\n");
    bzero(&servaddr, sizeof(servaddr));

    string choice;
    string ip;
    //Assumes Responder B is on thing2
    //ip = "10.35.195.46";     //thing0
    // ip = "10.35.195.47";    //thing1
    ip = "10.35.195.22";    //thing2
    // ip = "10.35.195.49";    //thing3
    // ip = "127.0.0.1";       //localhost

    // assign IP, PORT
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = inet_addr(ip.c_str());
    servaddr.sin_port = htons(PORT);

    // connect the client socket to server socket
    if (connect(sockfd, (SA *)&servaddr, sizeof(servaddr)) != 0)
    {
        printf("connection with the server failed...\n");
        exit(0);
    }
    else
        printf("connected to the server..\n");

    // function for chat
    func(sockfd, ks, ekb);

    // close the socket
    close(sockfd);
}
