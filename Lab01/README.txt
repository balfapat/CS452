By Patrick Balfanz, Jason Lake, and Will Johnson

Our program sucessfully completes the key distribution and transfers an ecrypted file from A to B

to run:
    run our makefile with "make" to compile 
    
    kdc, initA, and respB should all be run on separate thingys
        kdc on thing0 with ./kdc in the project directory,
        initA on thing1 with ./initA in the project directory,
        respB on thing 2 with ./respB in the project directory

        *kdc and respB should be run before initA as they listen for a connection from initA

design choices:
    For our file transfer, we decided to split the read in file into blocks. 
    This way, we don't have to worry about using too much memory at the same time.
    Currently, we read in 1MB of a file at a time, encrypt it, and send each block via TCP stream to respB in a char * buffer.
    respB continously calls recv() unil it grabs the designated amout of data, and puts it into it's own char * buffer for decryption.

    We have tested with a files up to 1G, which transfers in about 40s. We have also done 10G but only over localhost. 

    We also had to use multiple copies of blowfish becuase they didn't cooperate with having more than 
    one at a time. 

    The output file will always be transferred to /tmp/ of B

Sample run:

KDC:
-bash-4.2$ ./kdc
RUNNING DEBUG
Socket successfully created..
Socket successfully binded..
Server listening..
server acccept the client...
Please enter User A's private key: testAKey
Please enter User B's private key: testBKey
Please enter the session key: sessionKey

Recd from IDa:
Requesting Ks for (IDb)
N1 recd = 123456789

Send to IDa:
EKa[Ks||Req||N1||EKb(Ks, IDA)]: 5DD2F4...
Ks = sessionKey
N1 = 123456789

Initiator A:
-bash-4.2$ ./initA
RUNNING DEBUG
Socket successfully created..
connected to the server..
Please enter User A's private key: testAKey
Please enter a number: 123456789

Send to KDC:
Request:
Ks for (IDB)
N1 = 123456789

Received from KDC:
EKa[Ks||Req||N1||EKb(Ks, IDA)]: 5DD2F4...
Ks: sessionKey
N1: 123456789

Socket successfully created..
connected to the server..

Send to IDb:
Ekb[Ks,IDa]: 7532C6...

Recd from IDb:
Eks[N2]: ED596E...
N2 = 987654321
f(N2) = 22200

Send to IDb:
Eks[f(N2)]: 97983B...

Select a file to encrypt and send. Use the full path name: /tmp/1G
Transferring...
File Converted to hex:
945FD2...
Encrypted File Converted to hex:
0E1BC5...
Done.

Responder B:
-bash-4.2$ ./respB
RUNNING DEBUG
Socket successfully created..
Socket successfully binded..
Server listening..
server acccept the client...
Please enter User B's private key: testBKey
Please enter a number: 987654321

Recd from IDa:
Ekb[Ks,IDa]: 7532C6...
Ks = sessionKey
N2 = 987654321

Send to IDa:
Eks[N2]: ED596E...

Recd from IDa:
Eks[f(N2)]:97983B...
f(N2) = 22200
Validate (Recd f(N12) == calc f(N2)): 22200 == 22200

Successfully validated
Receiving File...

File received and creatd at: /tmp/Copy-1G
Encrypted file converted to hex:
0E1BC5...
File converted to hex:
945FD2...

md5sum: 
A: -bash-4.2$ md5sum /tmp/1G
    2ff956791133fd44985e1258f7040c37  /tmp/1G
B: -bash-4.2$ md5sum /tmp/Copy-1G
    2ff956791133fd44985e1258f7040c37  /tmp/Copy-1G


